# praktikum desain multiplayer online MiniAPP
4210181025_Jatayu Muhammad Wicaksono

![](Gif/a.gif)


**Memulai proses load link**


            `void Start()
            {
                WWW load = new WWW("https://5e510330f2c0d300147c034c.mockapi.io/users");
                StartCoroutine(LoadData(load));
                Debug.Log("link loaded");
            }`



**Display data**


            `private IEnumerator LoadData(WWW loader)
            {
                yield return loader;
                Debug.Log("Data Loaded");
                words.text = "Hi I am " + GetName(loader.text) + " contact me on email @" + GetEmail(loader.text) + ".";
            }`



**Load nama dari array ke25**


            `private string GetName(string json)
            {
                Debug.Log("data called");
                JSONArray array = JSON.Parse(json).AsArray;

                return array[25]["name"].Value;
                Debug.Log("name called");
            }`



**Load email dari array ke25**


            `private string GetEmail(string json)
            {
                Debug.Log("data called");
                JSONArray array = JSON.Parse(json).AsArray;

                return array[25]["email"].Value;
                Debug.Log("email called");
            }`
